// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <tuple>    // make_tuple, tie, tuple
#include <utility>  // make_pair, pair

#include "Collatz.hpp"

using namespace std;

// ------------
// collatz_read
// ------------

pair<int, int> collatz_read (const string& s) {
    istringstream sin(s);
    int i;
    int j;
    sin >> i >> j;
    return make_pair(i, j);
}

// ------------
// collatz_eval
// ------------

tuple<int, int, int> collatz_eval (const pair<int, int>& p) {
    long cycle_lengths [1000000];
    for(int i = 0; i < 1000000; i++) { // zero out cache
        cycle_lengths[i] = 0;
    }
// save i and j for printing output, and swap if i > j
    int i_f;
    int j_f;
    tie(i_f, j_f) = p;
    int i;
    int j;
    if(i_f > j_f) {
        i = j_f;
        j = i_f;
    }
    else {
        i = i_f;
        j = j_f;
    }
    // <your code>
    int max = 0;
    for(int temp = i; temp <= j; temp++) {
        int c = 1;
        long n = temp;
        while (n > 1) {
            //cout << n << endl;
            if(n < 1000000 && cycle_lengths[n] > 0) { // find cached value and leave loop if found
                c += cycle_lengths[n] - 1;
                break;
            }
            else {
                if ((n % 2) == 0)
                    n = (n / 2);
                else
                    n = (3 * n) + 1;
                ++c;
            }
        }
// cache when final count is found
        if(cycle_lengths[temp] == 0) {
            //cout << temp << " <-num, cycle length: ->";
            cycle_lengths[temp] = c;
            //cout << cycle_lengths[temp] << endl;
        }
        max = c > max ? c : max;
    }

    return make_tuple(i_f, j_f, max);
}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& sout, const tuple<int, int, int>& t) {
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    sout << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& sin, ostream& sout) {
    string s;
    while (getline(sin, s))
        collatz_print(sout, collatz_eval(collatz_read(s)));
}
