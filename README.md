# CS371p: Object-Oriented Programming Collatz Repo

* Name: Steven Sun

* EID: srs4698

* GitLab ID: ssun914

* HackerRank ID: stevensun914

* Git SHA: 4d486e5bbb4303a65743b98c0f57ceb94cc8ebdd

* GitLab Pipelines: https://gitlab.com/ssun914/cs371p-collatz/-/pipelines

* Estimated completion time: 0

* Actual completion time: 0

* Comments: This is a pasting of an old Collatz project from cs371g over last summer. Downing said no need for padding commits since 99% of the work was already done beforehand, and I'd assume same with issues. If these are required this is the link to my repo from last summer: https://gitlab.com/ssun914/cs371g-collatz
